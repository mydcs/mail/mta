<a name="unreleased"></a>
## [Unreleased]


<a name="0.0.1"></a>
## 0.0.1 - 2023-02-17
### Feat
- first release


[Unreleased]: https://gitlab.com/mydcs/mail/mta/compare/0.0.1...HEAD
