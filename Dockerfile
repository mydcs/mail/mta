FROM	alpine:3.17
LABEL	maintainer=Dis4sterRec0very

#
# Setup environment
#
ENV	DOMAIN=${DOMAIN:-example}
ENV	TLD=${TLD:-com}
ENV	ADMIN_PASSWORD=${ADMIN_PASSWORD:-secret}

#
# Install Dovecot
#
RUN apk --no-cache add \
	postfix \
	postfix-ldap \
	postfix-pcre \
	htop \
	openssl \
	openldap-clients

# files to image the easy way
COPY img_fs /
RUN chmod 777 /srv
RUN chmod 400 /etc/postfix/ldap/*

HEALTHCHECK --start-period=350s CMD echo QUIT|nc localhost 25|grep "220 .* ESMTP Postfix"

ENTRYPOINT ["./entrypoint.sh"]

EXPOSE 25/tcp 587/tcp
