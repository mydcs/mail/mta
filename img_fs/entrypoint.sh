#!/bin/sh

DOMAIN=${DOMAIN:-example}
TLD=${TLD:-com}
ADMIN_PASSWORD=${ADMIN_PASSWORD:-secret}
CIDR=$(ip route | grep src | grep eth0 | awk '{print $1}')

# Create virtual maps

sed -i "s#^bind_dn .*#bind_dn = cn=Manager,dc=$DOMAIN,dc=$TLD#g" /etc/postfix/ldap/*
sed -i "s#^bind_pw .*#bind_pw = $ADMIN_PASSWORD#g" /etc/postfix/ldap/*
sed -i "s#example.com#$DOMAIN.$TLD#g" /etc/postfix/ldap/*
sed -i "s#dc=example,dc=com#dc=$DOMAIN,dc=$TLD#g" /etc/postfix/ldap/*

postconf "myhostname=mail.$DOMAIN.$TLD"
postconf "mynetworks = 127.0.0.1/32 wpad.fritz.box $CIDR"

# some separate error codes
postconf "relay_domains_reject_code=564"
postconf "access_map_reject_code=574"
postconf "maps_rbl_reject_code=584"

# Relay to mailjet
postconf "relayhost = in-v3.mailjet.com"
postconf "relay_domains="
postconf "smtp_sasl_password_maps = lmdb:/srv/sasl_passwd"
postconf "smtp_sasl_auth_enable = yes"
postconf "smtp_sasl_security_options = noanonymous"
postmap -i lmdb:/srv/sasl_passwd < /srv/sasl_passwd

# transport to dovecot
postconf "virtual_transport=dovecot:inet:dovecot:24"
postconf -M "dovecot/inet=dovecot unix - - n - - lmtp"

#increase debug
postconf -M "smtp/inet=smtp inet n - n - - smtpd -v"
postconf "maillog_file=/dev/stdout"

# http://www.postfix.org/VIRTUAL_README.html#in_virtual_other
# Non-Postfix mailbox store: separate domains, non-UNIX accounts
# Lookup in LDAP
postconf "virtual_mailbox_domains=ldap:/etc/postfix/ldap/virtual_mailbox_domains.ldap"
postconf "virtual_mailbox_maps=ldap:/etc/postfix/ldap/virtual_mailbox_maps.ldap"
postconf "virtual_alias_maps=ldap:/etc/postfix/ldap/virtual_alias_maps.ldap"

# CHECKED UP TO THIS POINT
postconf "smtpd_recipient_restrictions = permit_mynetworks, reject_unauth_pipelining, reject_non_fqdn_recipient, reject_invalid_helo_hostname, reject_unknown_recipient_domain, reject_unauth_destination"

postconf "smtpd_helo_required=yes"
postconf "smtpd_helo_restrictions=permit_mynetworks, reject_invalid_helo_hostname, reject_non_fqdn_helo_hostname, reject_unknown_helo_hostname"

postconf "smtpd_data_restrictions=reject_unauth_pipelining, reject_multi_recipient_bounce, permit"

# TLS
postconf "smtpd_tls_cert_file = /etc/ssl/letsencrypt/fullchain.pem"
postconf "smtpd_tls_key_file = /etc/ssl/letsencrypt/privkey.pem"
postconf "smtpd_tls_loglevel = 1"
postconf "smtpd_tls_received_header = yes"
postconf "smtpd_tls_security_level = may"
postconf "smtpd_tls_auth_only = yes"
postconf "smtpd_tls_mandatory_protocols = !SSLv2, !SSLv3"
#postconf "tls_preempt_cipherlist = yes"
postconf "tls_disable_workarounds = 0xFFFFFFFFFFFFFFFF"
postconf "smtpd_tls_mandatory_ciphers = high"
postconf "smtpd_tls_exclude_ciphers = aNULL, eNULL, EXPORT, DES, RC4, MD5, PSK, aECDH, EDH-DSS-DES-CBC3-SHA, EDH-RSA-DES-CDB3-SHA, KRB5-DES, CBC3-SHA"
postconf "smtpd_tls_dh1024_param_file = /etc/ssl/letsencrypt/ssl-dhparams.pem"
postconf "smtpd_tls_eecdh_grade = ultra"

# Setup directories
if [ ! -d /srv/queue ]; then
    mkdir /srv/queue
    chown postfix:postfix /srv/queue
fi
if [ ! -d /srv/data ]; then
    mkdir /srv/data
    chown postfix:postfix /srv/data
fi

postconf "queue_directory = /srv/queue"
#postconf "data_directory = /srv/data"

# enable sasl authentication to dovecot
postconf "smtpd_sasl_type=dovecot"
postconf "smtpd_sasl_auth_enable=yes"
postconf "smtpd_sasl_path=inet:dovecot:14322"
postconf "smtpd_recipient_restrictions = permit_mynetworks, permit_sasl_authenticated, reject_unauth_pipelining, reject_non_fqdn_recipient, reject_invalid_helo_hostname, reject_unknown_recipient_domain, reject_unauth_destination"

postconf "smtpd_milters=inet:rspamd:11332"
postconf "non_smtpd_milters=inet:rspamd:11332"
postconf milter_mail_macros="i {mail_addr} {client_addr} {client_name} {auth_authen}"

postfix start-fg