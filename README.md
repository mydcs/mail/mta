# Status

[gitlab-pipeline-image]: https://gitlab.com/mydcs/mail/mta/badges/main/pipeline.svg
[gitlab-pipeline-link]: https://gitlab.com/mydcs/mail/mta/-/commits/main
[gitlab-release-image]: https://gitlab.com/mydcs/mail/mta/-/badges/release.svg
[gitlab-release-link]: https://gitlab.com/mydcs/mail/mta/-/releases
[gitlab-stars-image]: https://img.shields.io/gitlab/stars/mydcs/mail/mta?gitlab_url=https%3A%2F%2Fgitlab.com
[gitlab-stars-link]: https://hub.docker.com/r/mydcs/mail/mta

[docker-pull-image]: https://img.shields.io/docker/pulls/mydcs/mta.svg
[docker-pull-link]: https://hub.docker.com/r/mydcs/mta
[docker-release-image]: https://img.shields.io/docker/v/mydcs/mta?sort=semver
[docker-release-link]: https://hub.docker.com/r/mydcs/mta
[docker-stars-image]: https://img.shields.io/docker/stars/mydcs/mta.svg
[docker-stars-link]: https://hub.docker.com/r/mydcs/mta
[docker-size-image]: https://img.shields.io/docker/image-size/mydcs/mta/latest.svg
[docker-size-link]: https://hub.docker.com/r/mydcs/mta


[![gitlab-pipeline-image]][gitlab-pipeline-link] 
[![gitlab-release-image]][gitlab-release-link]
[![gitlab-stars-image]][gitlab-stars-link]


[![docker-pull-image]][docker-pull-link] 
[![docker-release-image]][docker-release-link]
[![docker-stars-image]][docker-stars-link]
[![docker-size-image]][docker-size-link]

# How To

## Vorbereitung

1. Arbeitsverzeichnis anlegen
2. In das Arbeitsverzeichnis wechseln
3. Unterordner config anlegen
4. Container starten, entweder ueber "docker run" oder "docker compose"

## Aufruf

### CLI

```
docker run -it --rm -e DOMAIN=example -e TLD=com -v $(pwd)/config:/srv -v /etc/letsencrypt:/etc/letsencrypt mydcs/mta:latest
```

### Compose

```dockerfile
version: "3.8"

services:
  mta:
    container_name: mta
    hostname: mta
    restart: always
    image: mydcs/mta:latest
    environment:
      - DOMAIN=${DOMAIN:-example}
      - TLD=${TLD:-com}
    networks:
      - dcs
    ports:
      - 25:25
      - 587:587
    volumes:
      - $PWD/config:/srv
      - /etc/letsencrypt:/etc/letsencrypt

networks:
  dcs:
    name: dcs
```

