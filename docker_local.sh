#!/bin/bash

export NAME=$(basename $PWD)
export TAG=latest

docker build -t $NAME:$TAG .

docker run -it --rm \
    $* \
    -p 25:25 \
    -p 587:587 \
    --network dcs \
    --name $NAME \
    --hostname $NAME \
    $NAME:$TAG

#docker image prune -a -f
